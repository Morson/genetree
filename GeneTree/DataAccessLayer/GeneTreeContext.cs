﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneTree.DataAccessLayer
{
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class GeneTreeContext:DbContext
    {
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Category> Categories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=GeneTree;Trusted_Connection=True;");
        }
    }
}
