﻿namespace GeneTree.DataAccessLayer
{
    using Models;
    using Repositories;

    public class UnitOfWork
    {
        private readonly GeneTreeContext _context;

        public IGenericRepository<Person> People { get; }
        public IGenericRepository<Category> Categories { get; }

        public UnitOfWork(GeneTreeContext context)
        {
            _context = context;
            People = new GenericRepository<Person>(_context);
            Categories = new GenericRepository<Category>(_context);
        }

        public void Complete()
        {
            _context.SaveChanges();
        }
    }
}
