﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneTree
{
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;

    public static class Extensions
    {
        public static bool HasRubbish(this string text)
        {
            var rubbishPattern = @"\d{1,8}/{1}\d{1,8}$";
            return Regex.IsMatch(text, rubbishPattern);
        }

        public static int ToInt(this string text)
        {
            if (int.TryParse(text, out var result))
                return result;
            return -1;
        }

        public static string GetMenuName(this Action action)
        {
            var mName = ((MenuName) action.Method.GetCustomAttributes(typeof(MenuName), false)[0]);
            if (mName != null) return mName.Name;
            return string.Empty;
        }

        public static string FirstLine(this string pageText)
        {
            return pageText.Split("\n").First();
        }

        public static string SecondLine(this string pageText)
        {
            return pageText.Split("\n")[1];
        }

        public static bool IsUpperOnly(this string textLine)
        {   
            //var textLineUpperOnly = textLine.ToUpper();
            return string.Equals(textLine,textLine.ToUpper());
        }
    }
}
