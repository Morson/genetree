﻿namespace GeneTree
{
    using System;
    using System.Collections.Generic;

    public class Menu
    {
        private readonly string _title;
        private readonly Dictionary<string, Action> _options;
        private readonly string _closeOption;
        
        public Menu(Dictionary<string, Action> options,string closeOption,string title)
        {
            _options = options;
            _closeOption = closeOption;
            _title = title;
        }
        
        public void Show()
        {
            do
            {
                ShowTitle();
                ShowOptions();
                var opt = GetUserResponse();

                if (opt == _closeOption) break;
                if (_options.TryGetValue(opt, out var value))
                    value.Invoke();
                else
                {
                    Console.WriteLine("Wybór nieprawidłowy");
                    Console.WriteLine();
                }

            } while (true);
        }

        private void ShowOptions()
        {
            foreach (var option in _options)
            {
                Console.WriteLine($"{option.Key}   - {option.Value.GetMenuName()}");
            }
        }

        private string GetUserResponse()
        {
            var result = Console.ReadKey(true).KeyChar.ToString().ToUpper();
            Console.Clear();
            return result;
        }

        private void ShowTitle()
        {
            Console.WriteLine(_title);
            SpecialEffects3D();
        }

        private static void SpecialEffects3D()
        {
            Console.WriteLine("--------------------");
            Console.WriteLine("-------MENU---------");
            Console.WriteLine("--------------------");
        }
    }
}