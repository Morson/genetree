﻿namespace GeneTree
{
    using System;

    [AttributeUsage(AttributeTargets.Method,Inherited = false)]
    public class MenuName : Attribute
    {
        public MenuName(/*string number,*/ string name)
        {
            //Number = number;
            Name = name;
        }

        //public string Number { get; }
        public string Name { get; }
    }
}