﻿namespace GeneTree.Models
{
    using System;

    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public int StartYear { get; set; }
        public int EndYear { get; set; }
        public int StartSite { get; set; }
        public int EndSite { get; set; }

        public void InitDataByPageContent(string textFromPage)
        {
            var textLines = textFromPage.Split(new []{"\n","-"},StringSplitOptions.RemoveEmptyEntries);
            Name = textLines[0];
            StartYear = textLines[1].ToInt();
            EndYear = textLines[2].ToInt();
        }
    }
}
