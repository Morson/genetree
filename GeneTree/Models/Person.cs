﻿namespace GeneTree.Models
{
    public class Person
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public int YearOfBirth { get; set; }
    }
}
