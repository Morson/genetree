﻿namespace GeneTree
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using DataAccessLayer;
    using iText.Kernel.Pdf;
    using iText.Kernel.Pdf.Canvas.Parser;
    using iText.Kernel.Pdf.Canvas.Parser.Listener;
    using Models;

    public class PdfWrapper
    {
        private readonly string _fileName = "tree.pdf";
        private readonly PdfDocument _pdfDocument;
        public int NumberOfPages => _pdfDocument.GetNumberOfPages();
        public int FirstPage => 3;
        private UnitOfWork _unitOfWork;
        
        public PdfWrapper()
        {
            var targetPath = _getTargetPath(Directory.GetCurrentDirectory());
            var pdfReader = new PdfReader(targetPath);
            _pdfDocument = new PdfDocument(pdfReader);
            _unitOfWork = new UnitOfWork(new GeneTreeContext());
        }

        [MenuName("Dodaj kategorie do bazy danych")]
        public void AddCategoriesToDb()
        {
            var categories = FindCategoryNames();
            FillCategoryDates(categories);
            FillLastPages(categories);
            RemoveUnnescesarryPages(categories);
            if (!_unitOfWork.Categories.Any())
            {
                _unitOfWork.Categories.AddRange(categories);
                _unitOfWork.Complete();
            }   
        }

        [MenuName("Dodaj ochrzczonych")]
        public void AddBaptizedToDb()
        {
            var baptisms = _unitOfWork.Categories.FindBy(cat => cat.Name == "CHRZTY").First();
            var siteNumber = baptisms.StartSite;
            var selectedYear=0;
            var person = string.Empty;
            var people = new List<string>();
            var peopleYear = new Dictionary<int,IEnumerable<string>>();

            do
            {
                var site = GetTrimmedPage(siteNumber);
                var textLines = site.Split("\n",StringSplitOptions.RemoveEmptyEntries).Where(line => line!=" ").ToList();
                var yearSearchExpression = "Chrzty w par. Łaskarzew";
                
                foreach (var textLine in textLines)
                {
                    if (Regex.IsMatch(textLine, yearSearchExpression))
                    {
                        if (!string.IsNullOrEmpty(person))
                        {
                            people.Add(person);
                            peopleYear.Add(selectedYear,new List<string>(people));

                            person = string.Empty;
                            people.Clear();
                        }

                        var newSelectedYear = Regex.Match(textLine, "\\d{4}").Captures.First().Value.ToInt();
                        if (newSelectedYear<=selectedYear)
                        {
                            newSelectedYear=selectedYear+1;
                        }

                        selectedYear = newSelectedYear;
                    }
                    else
                    {
                        if(selectedYear==0)continue;
                        if (Regex.IsMatch(textLine, "^\\d+."))
                        {
                            if (!string.IsNullOrEmpty(person))
                            {
                                people.Add(person);
                            }
                            person = textLine;
                        }
                        else
                        {
                            person += " " + textLine;
                        }
                    }
                }

                siteNumber++;
            } while (siteNumber<=baptisms.EndSite);
            
            peopleYear.Add(selectedYear,new List<string>(people));
        }

        private void FillCategoryDates(List<Category> categories)
        {
            foreach (var category in categories)
            {
                var dates = GetTrimmedPage(category.StartSite).SecondLine().Split("-");
                category.StartYear = dates[0].ToInt();
                category.EndYear = dates[1].ToInt();
            }
        }

        private void FillLastPages(List<Category> categories)
        {
            var searchedCategoryIndex = 0;
            var lastIndex = categories.Count-1;
            do
            {
                categories[searchedCategoryIndex].EndSite = categories[searchedCategoryIndex+1].StartSite-1;
                searchedCategoryIndex++;
            } while (searchedCategoryIndex<lastIndex);

            categories[lastIndex].EndSite = NumberOfPages - 1;
        }
        private void RemoveUnnescesarryPages(List<Category> categories)
        {
            foreach (var category in categories)
            {
                FindLastRequiredPage(category);
            }
        }

        private void FindLastRequiredPage(Category category)
        {
            for (int pageNumber = category.EndSite; pageNumber >= category.StartSite; pageNumber--)
            {
                if (!GetTrimmedPage(pageNumber).HasRubbish())
                {
                    category.EndSite = pageNumber;
                    break;
                }
            }
        }

        private List<Category> FindCategoryNames()
        {
            var categories = new List<Category>();
            for (int pageNumber = FirstPage; pageNumber < NumberOfPages; pageNumber++)
            {
                var pageFirstLine = GetTrimmedPage(pageNumber).FirstLine();
                if (pageFirstLine.IsUpperOnly())
                {
                    var category = new Category {Name = pageFirstLine, StartSite = pageNumber};
                    categories.Add(category);
                }
            }

            return categories;
        }

        private string GetTrimmedPage(int pageNumber)
        {
            var unnecessarySigns = new[] {' ', '\n'};
            return GetRawPage(pageNumber)
                .Trim(unnecessarySigns)
                .Replace("…","");
        }

        private string GetRawPage(int pageNumber)
        {
            var page = _pdfDocument.GetPage(pageNumber);
            return PdfTextExtractor.GetTextFromPage(page);
        }

        private string _getTargetPath(string currentDirectory)
        {
            var cDirInfo = new DirectoryInfo(currentDirectory);
            var filePath = cDirInfo
                .Parent?.Parent?.Parent
                ?.FullName;

            return Path.Combine(filePath, _fileName);
        }
    }
}