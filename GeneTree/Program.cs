﻿namespace GeneTree
{
    using System;
    using System.Collections.Generic;

    public class Program
    {
        public static void Main(string[] args)
        {
            var pdf = new PdfWrapper();
            var dict = new Dictionary<string, Action>();
            var title = "Drzewo Genealogiczne";

            dict.Add("1",pdf.AddCategoriesToDb);
            dict.Add("2",pdf.AddBaptizedToDb);
            var menu = new Menu(dict,"K",title);
            menu.Show();

        }
    }
}
