﻿namespace GeneTree.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using DataAccessLayer;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking;

    public class GenericRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : class
    {
        protected readonly GeneTreeContext Ctx;
        protected readonly DbSet<TEntity> GeneDbSet;

        public GenericRepository(GeneTreeContext ctx)
        {
            Ctx = ctx;
            GeneDbSet = ctx.Set<TEntity>();
        }

        public virtual TEntity GetById(object id)
        {
            return GeneDbSet.Find(id);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return GeneDbSet.AsQueryable<TEntity>();
        }

        public virtual bool Any()
        {
            return GetAll().Count() != 0;
        }

        public IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            return GeneDbSet.Where(predicate).AsQueryable();
        }

        public virtual EntityEntry<TEntity> Add(TEntity entity)
        {
            return GeneDbSet.Add(entity);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities)
        {
            GeneDbSet.AddRange(entities);
        }

        public virtual EntityEntry<TEntity> Delete(TEntity entity)
        {
            return GeneDbSet.Remove(entity);
        }

        public virtual void Edit(TEntity entity)
        {
            Ctx.Entry(entity).State = EntityState.Modified;
        }

        public virtual bool Contains(TEntity entity)
        {
            return GeneDbSet.Contains(entity);
        }

        public virtual void Save()
        {
            Ctx.SaveChanges();
        }
    }
}
