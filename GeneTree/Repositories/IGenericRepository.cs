﻿namespace GeneTree.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Microsoft.EntityFrameworkCore.ChangeTracking;

    public interface IGenericRepository<TEntity> where TEntity : class
    {
        TEntity GetById(object id);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate);
        EntityEntry<TEntity> Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        EntityEntry<TEntity> Delete(TEntity entity);
        bool Contains(TEntity entity);
        bool Any();
        void Edit(TEntity entity);
        void Save();
    }
}